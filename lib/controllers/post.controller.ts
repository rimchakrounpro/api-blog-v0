// lib/controllers/nodes.controller.ts
import { Request, Response } from "express";
import { Post, PostInterface } from "../models/post.model";
import { DestroyOptions, UpdateOptions } from "sequelize/types";
export class PostController {
  public index(req: Request, res: Response) {
    Post.findAll<Post>({})
    .then((posts: Array<Post>) => res.json(posts))
    .catch((err: Error) => res.status(500).json(err));
  }
  public create(req: Request, res: Response) {
    const params: PostInterface = req.body;
    Post.create<Post>(params)
      .then((post: Post) => res.status(201).json(post))
      .catch((err: Error) => res.status(500).json(err));
  }


  public show(req: Request, res: Response) {
    const postId: number = +req.params.id;

    Post.findByPk<Post>(postId)
      .then((post: Post | null) => {
        if (post) {
          res.json(post);
        } else {
          res.status(404).json({ errors: ["Node not found"] });
        }
      })
      .catch((err: Error) => res.status(500).json(err));
  }
  public update(req: Request, res: Response) {
    const postId: number = +req.params.id;
    const params: PostInterface = req.body;

    const update: UpdateOptions = {
      where: { id: postId },
      limit: 1,
    };
 
    Post.update(params, update)
      .then(() => res.status(202).json({ data: "success" }))
      .catch((err: Error) => res.status(500).json(err));
  }


  public delete(req: Request, res: Response) {
    const  postId: number = +req.params.id;
    const options: DestroyOptions = {
      where: { id: postId },
      limit: 1,
    };

    Post.destroy(options)
      .then(() => res.status(204).json({ data: "success" }))
      .catch((err: Error) => res.status(500).json(err));
  }
  
}