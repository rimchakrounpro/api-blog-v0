// lib/config/database.ts
import { Sequelize } from "sequelize";
import * as dotenv from "dotenv";
dotenv.config();
const DBName=process.env['DATABASE_NAME'];

const DBUsername=process.env.DATABASE_USERNAME;
const DBPassword=process.env.DATABASE_PASSWORD;
const DBPort=process.env.DATABASE_PORT;
export const database = new Sequelize(DBName,DBUsername,DBPassword,{
  dialect: "mysql"
});