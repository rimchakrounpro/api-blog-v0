import { UserController } from "../controllers/user.controller";

import { PostController } from "../controllers/post.controller";

export class Routes {
  public userController: UserController = new UserController();
  public postController: PostController = new PostController();
  public routes(app): void {
    app.route("/users").get(this.userController.index).post(this.userController.create);
    app.route("/users/:id").get(this.userController.show).
                            put(this.userController.update).
                            delete(this.userController.delete);
    app.route("/posts").get(this.postController.index).post(this.postController.create);



  }}