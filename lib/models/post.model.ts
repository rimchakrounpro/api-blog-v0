  // lib/models/node.model.ts
import { userInfo } from "os";
import { Sequelize, Model, DataTypes, BuildOptions } from "sequelize";
import { database } from "../database";
import { User} from "./user.model";

export class Post extends Model {
  public id!: number;
  public title!: string;
  public content!:string;
  public userId!:number;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}
export interface PostInterface {
    title: string;
    content:string;
    userId:number;
}

Post.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      title: {
        type: new DataTypes.STRING(128),
        allowNull: false,
      },
      content: {
        type: new DataTypes.STRING(128),
        allowNull: false,
      },
      userId: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull:false,
      
      },
    },
    {
      tableName: "posts",
      sequelize: database, 
    }
  );
  Post.belongsTo(User,{foreignKey: 'userId',targetKey: 'id'});

 
  Post.sync({ force: true }).then(() => console.log("Posts table created"));