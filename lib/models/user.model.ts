// lib/models/node.model.ts
import { Model, DataTypes } from "sequelize";
import { database } from "../database";

export class User extends Model {
  public id!: number;
    public username!: string;
  public password!:string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

}
export interface CreateUserInterface {
  username: string;
  password:string;
}
export interface UserInterface {
  username: string;
}
User.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      username: {
        type: new DataTypes.STRING(128),
        unique:true,
        allowNull: false,
      },
      password: {
        type: new DataTypes.STRING(255),
        allowNull: false,
      },
    },
    {
      tableName: "users",
      sequelize: database, 
    }
  );
 
  User.sync({ force:true}).then(() => console.log("Users table created"));