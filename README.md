# API Blog
L'idée est de créer une Api pour un blog sur le passage des titres pro.
L’intérêt principal de ce blog est de partager des expériences, des conseils, des moments de vie autour du passage de titre pro.

## Pour commencer
Cette Api devra contenir:
- Un module d'authentification (inscription, login compris)
- Un module de gestion de posts

## Techno
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/made-with-typescript.svg)](https://forthebadge.com)

- Nodejs Express
- TypeScript
- Mysql
- sequelize

## Installation
Cloner le projet puis executer la commande `` npm install``

## Démarrage
- creer une base de données en utilisant phpMyAdmin 
- Copier le fichier **.env-exemple** 
- Renommer le fichier **.env**
- Remplir le fichier par les vrai info de connexion
- Ne pas oublier d'enregister  :yum:
- Executer la commande ``npm run dev``
## Versions
Listez les versions ici 
_exemple :_
**Dernière version stable :** 5.0
**Dernière version :** 5.1
Liste des versions : [Cliquer pour afficher](https://github.com/your/project-name/tags)
_(pour le lien mettez simplement l'URL de votre projets suivi de ``/tags``)_

